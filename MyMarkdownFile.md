20. Princess Bubblegum
Princess Bubblegum lands at the bottom of this list because she's probably the least naturally-gifted fighter, and she doesn't show many signs of great strength over the course of the show. She employs Finn and Jake to be her muscle in the Candy Kingdom and has a royal guard to help out as well. Still, Bubblegum deserves a spot here because of her intelligence and leadership skills.  She also develops elemental powers, but she never gets a great handle on them.

19. Ice King
Ice King has significant magical abilities that he can deploy in a variety of ways, but what makes him land so low on this list is that he consistently gets beaten by a human teenager with nothing but a sword and a strong will.

18. Finn
Finn is a brave hero. He saves princesses, serves kingdoms, and always strives to do the right thing. But he's still just a human in a world full of magical creatures. What makes Finn land above Princess Bubblegum and Ice King isn't any special fighting ability, but his determination.

17. Lady Rainicorn
Lady Rainicorn doesn't get a lot of time to shine in "Adventure Time," but when she does, she employs a wide range of powers in a variety of ways, easily earning her a spot on this list. Her tenacity — especially when it comes to her family –also counts for a lot.

16. Magic Man and Magic Betty
In Magic Man's first appearance on "Adventure Time" in the episode "Freak City," he turns Finn into a giant foot. We learn that he's also turned a number of other inhabitants of Ooo into strange body part creatures, so it seems pretty clear that he's a strong character from the start. Later, when Magic Man's powers are transferred to Betty in "You Forgot Your Floaties," we see these powers grow. Betty is ferocious in her quest to help Ice King return to his former self as Simon, and she uses her newfound abilities in impressive ways.

15. Huntress Wizard
Huntress Wizard is another character, like Lady Rainicorn and others on this list, who doesn't get much screen time over the 10 seasons of "Adventure Time." However, she makes every appearance count. She's perhaps the coolest character in the show due to her laid-back confidence and her many skills, and she's certainly also one of the strongest.

14. Susan Strong
It might be obvious, but of course Susan Strong was going to end up on this list of the strongest "Adventure Time" characters. "Strong" is literally her name. Beyond that, though, we can safely say that Susan — also known as Kara (her original island name from before she lost her memories) and Seeker XJ-7-7 — is simply one of the most powerful characters in the show because of her literal strength. In fact, Susan may be the most purely strongest character in all of "Adventure Time," especially after her transformation into an even larger and more powerful version of herself in "Reboot."

13. Fern/The Green Knight
Fern is essentially just a magically souped-up version of Finn, born of a merging of the Grass Sword and the Finn Sword as seen in "Reboot." At first, Fern is just Finn with all of the magical powers of the Grass Sword imbued into his being, as we see in "Do No Harm." Later, after Fern is transformed into the Green Knight by Uncle Gumbald between "Three Buckets" and "Seventeen," he's even more powerful. But it's not only Fern's physical strength and magic that land him on this list. It's also his cunning.

12. Jake
Finn and Jake may be equally regarded as heroes, and as we see in "Davey," Finn might actually be even more popular. But when it comes down to it, Jake is the more competent of the two because of his stretchy powers and his resilience. The episode "Dungeon" in Season 1 even acknowledges this with a wink when Jake tells Finn that he won't be able to complete a dungeon without Jake's help. Of course, Jake then runs into his own challenges, like being asked to make a choice between weapons, spitting far, and needing to not get distracted by a laser pointer. As it turns out, each of the two needs the other.

11. Billy
In "His Hero," the first episode in which we meet Billy, he's described in a song as "the greatest warrior ever." We hear about his exploits, which include slaying an evil ocean, casting the Lich King down, saving a damsel from a Firecount, and also fighting a bear. Billy may not have magical powers, but of the non-magical characters in "Adventure Time," it's pretty clear that he's the strongest. After all, he's a warrior of legend who's considered to be the greatest ever.

10. Patience St. Pim
Patience St. Pim is the main reason that the "Elements" miniseries happens in "Adventure Time." She introduces the concept of elementals to the lore of the show, and she's also an incredibly powerful villain. When she's first introduced in "Elemental," she easily incapacitates Finn and Jake, showing that her ice powers are far greater than Ice King's. We also learn that she froze herself for thousands of years, exhibiting a strength of will that's equally impressive and frightening.

9. Peppermint Butler
For casual fans of "Adventure Time," it may seem odd that Princess Bubblegum's butler lands so high on this list of the strongest characters in the show. But despite his adorable appearance and manservant day job — one he is deeply committed to — Peppermint Butler really is one of the strongest characters in the land of Ooo. He's not very physically strong to be sure, but he's a powerful magic user and practitioner of the dark arts.

8. Flame Princess
Flame Princess is without a doubt the strongest of the elementals, even more powerful than Patience St. Pim. First off, she's literally made of fire and can transform herself into a giant raging flame, as we see in "Incendium" (her first appearance) and "Frost & Fire." In her fight with Ice King in "Frost & Fire," we also get to see her use a powerful flame attack that's basically a kamehameha of concentrated fire. And in "Vault of Bones," she displays the ability to create a small but extremely hot blue flame that she can wield as a sword or precision weapon. We also learn that she has a "heat sense" that allows her to locate things that are out of place based on their heat signatures.

7. Vampire King
While some of the other vampires from the "Stakes" miniseries could arguably also belong on this list, the only one who has to be here is their leader: The Vampire King. Throughout the miniseries, his significant powers are put on display. These include telekinesis, which allows him to lift and choke other characters, as seen in "Vamps About," and destroy stakes being used to attack him in "Checkmate." "Checkmate" also shows us that he has the ability to teleport both himself and others, and that he can read minds and speak to people telepathically.

6. Hunson Abadeer
In Hunson Abadeer's first episode, "It Came from the Nightosphere," it's revealed that he literally cannot die or be killed. He's a deathless being, so that immediately lands him pretty high on a list of the strongest characters in the show. But more than simply being unkillable, he also displays a variety of powers over the course of the series, and his title as Lord of Evil and ruler of the Nightosphere certainly helps cement him as one of the strongest characters as well.

5. Marceline
Marceline the Vampire Queen was born half-human and half-demon. Over the course of her vampire-hunting — both in the past and again throughout "Stakes" — she uses her inherited soul-sucking power to absorb the powers of the vampires she kills. Finally, she's turned into an immortal vampire herself after being bitten by the Vampire King (as seen in "May I Come In") and absorbing his pure vampire essence (shown in "The Dark Cloud"). To say the very least, she's got a lot going on when it comes to the "strongest" conversation.

4. The Lich
The Lich is the embodiment of the death of all things, so of course he lands high on this list of the strongest characters in "Adventure Time." Yes, he's defeated by characters much lower on this ranking like Billy (as we learn in "His Hero") and Finn (in "Mortal Folly"), but because he's a cosmic entity, he keeps on returning even when he seems to have been defeated. This longevity also plays a major role in the Lich's high ranking here.

3. Prismo
Like the Lich and Hunson Abadeer, Prismo simply refuses to die. It certainly helps that he's another cosmic entity — a Wishmaster to be exact — who's able to manipulate time and space so that when he does die, he comes right back to life. The complex time manipulations through which he accomplishes this are seen in "Is That You?"

2. Gunter/Orgalorg
Gunter is a veteran of physical combat (or at least violence), whether that be casually tasing Ice King and Finn in "Princess Potluck," breaking bottles in "Still" and "Reign of Gunters," or fearlessly slapping Hunson Abadeer after being named "by far the most evil thing [he's] encountered" in "It Came from the Nightosphere." Hunson's comment seems to be a simple joke, until much later when it's revealed that Gunter is in fact the cosmic entity known as Orgalorg in the episode "Orgalorg."

1. GOLB
GOLB only significantly appears in one episode of "Adventure Time," but it's the extended series finale "Come Along with Me," so it certainly makes an impact. GOLB surpasses the cosmic entities that make up the last few entrants on this list and is in fact a deity. GOLB is the embodiment of chaos, GOLB is pure discord, and as Simon explains in a flashback in "Come Along With Me," its "presence is felt in every crevice where chaos lurks." A testament to GOLB's power is that the Lich refers to himself as "the last scholar of GOLB" in "Whispers," so GOLB is the ancient evil that the embodiment of all death worships and studies.

